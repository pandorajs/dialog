# pandora-dialog [![spm version](http://127.0.0.1:3000/badge/pandora-dialog)](http://127.0.0.1:3000/package/pandora-dialog)

---



## Install

```
$ spm install pandora-dialog --save
```

## Usage

```js
var pandoraDialog = require('pandora-dialog');
// use pandoraDialog
```
