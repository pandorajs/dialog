module.exports = require('./lib/dialog');
module.exports.Tips = require('./lib/tips');
module.exports.Confirm = require('./lib/confirm');
module.exports.Alert = require('./lib/alert');
